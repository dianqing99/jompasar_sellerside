package com.example.jompasarseller;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class review_list_adapter extends FirestoreRecyclerAdapter<review_model, review_list_adapter.reviewViewHolder> {

    FirebaseStorage fStore;
    StorageReference storeRef;
    //private review_list_adapter.OnListItemClick onListItemClick;
    Context context;

    //Constructor
    public review_list_adapter(Context c, @NonNull FirestoreRecyclerOptions<review_model> options) {
        super(options);
        //this.onListItemClick = onListItemClick;
        this.context = c;
    }

    @Override
    protected void onBindViewHolder(@NonNull review_list_adapter.reviewViewHolder holder, int position, @NonNull review_model model) {
        //Firebase Variable
        storeRef= FirebaseStorage.getInstance().getReference();
        String seller_id = getSnapshots().getSnapshot(position).getId();

        //Timestamp Converstion
        Timestamp timestamp = model.getTimestamp();
        Date date = timestamp.toDate();
        String times = new SimpleDateFormat("yyyy-MM-dd").format(date);

        //Display the Data
        holder.review_list_name.setText(model.getBuyer_name());
        holder.review_list_desc.setText(model.getComment());
        holder.review_list_rating.setText(String.valueOf(model.getRating()));
        holder.review_list_time.setText(times);

    }

    @NonNull
    @Override
    public review_list_adapter.reviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review,parent,false);
        return new reviewViewHolder(view);

    }

    public class reviewViewHolder extends RecyclerView.ViewHolder {

        private TextView  review_list_name;
        private TextView  review_list_desc;
        private TextView  review_list_rating;
        private TextView  review_list_time;

        public reviewViewHolder(@NonNull View itemView) {
            super(itemView);

                //XML hook to Java Code;
                review_list_name = itemView.findViewById(R.id.review_buyer_name);
                review_list_desc = itemView.findViewById(R.id.review_pricetext);
                review_list_rating = itemView.findViewById(R.id.review_rating);
                review_list_time = itemView.findViewById(R.id.review_timestamp);

        }




       /* @Override
        public void onClick(View v) {
            //This will pass the data to MainActivity;
//            onListItemClick.onItemClick(getItem(getAdapterPosition()),getAdapterPosition(), getSnapshots());
            onListItemClick.onItemClick(getSnapshots().getSnapshot(getAdapterPosition()).getId());
        }*/
    }

 /*   //Interface used for the MainActivity
    public interface OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String merchantId);
    }

*/



}
