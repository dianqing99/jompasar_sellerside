package com.example.jompasarseller;

public class seller_menu_model {
    private String id;
    private String description;
    private String img_url;
    private String name;
    private Double price;
    //private HelperClass helper = new HelperClass();

    public seller_menu_model() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    /*public String getPriceRM(){
        return "RM " + helper.roundPriceRM(price);
    }*/

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}


