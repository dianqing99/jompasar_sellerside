package com.example.jompasarseller;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class review_list extends AppCompatActivity{

    //Global Variable
    String seller_id;

    //Firebase Variable
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    StorageReference storeRef;


    //Firestore UI Variable
    private RecyclerView fireStoreReviewList;
    private review_list_adapter adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);

        //Firebase Variable Initialization
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        seller_id = fAuth.getCurrentUser().getUid();

        //Firestore UI RecycleView Logic
        //Query and Variable Initialization
        fireStoreReviewList = findViewById(R.id.review_list);
        Query query = fStore.collection("reviews/" ).whereEqualTo("seller_id",seller_id);

        //Recycler Options
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<review_model>()
                .setQuery(query,review_model.class)
                .build();

        adapter = new review_list_adapter(this,options);


        //View Holder Class
        fireStoreReviewList.setHasFixedSize(true);
        fireStoreReviewList.setLayoutManager(new LinearLayoutManager(this));
        fireStoreReviewList.setAdapter(adapter);


    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


   /* @Override
    public void onItemClick(String orderID) {

    }*/
}