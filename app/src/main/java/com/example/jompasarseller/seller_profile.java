package com.example.jompasarseller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class seller_profile extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navView;

    //private RecyclerView sellerData;
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth;
    StorageReference storeRef;


    private TextView userName;
    private TextView shopName;
    private TextView address;
    private TextView phone;
    private TextView email;
    private TextView city;
    private TextView description;
    private ImageView profileImg;

    private Button editProfile;
    private Button ItemList;

    private String sellerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_profile);


        //Instantiate Firebase Variable

        firebaseFirestore = FirebaseFirestore.getInstance();
        fAuth =FirebaseAuth.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();

        //XML Bind
        editProfile = (Button) findViewById(R.id.edit_profile_page);

        profileImg = findViewById(R.id.seller_image1);

        ItemList=(Button)findViewById(R.id.seller_item_page);


        seller_model seller_pass = new seller_model();

        //get user uid and parse into string
        sellerID = fAuth.getCurrentUser().getUid().toString();



        //Code for Displaying Image
        StorageReference profileImgRef = storeRef.child("/seller/" + fAuth.getCurrentUser().getUid()
                + "/seller_img.jpg");
        profileImgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(profileImg);
            }
        });

        //locate specific seller data file
        DocumentReference docRef = firebaseFirestore.collection("sellers").document(sellerID);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                //pass the seller data get from database into a empty seller object
                seller_model seller = documentSnapshot.toObject(seller_model.class);

                //sign the variable above to respective view in design

                userName = findViewById(R.id.user_name);
                shopName = findViewById(R.id.shop_name);
                address = findViewById(R.id.seller_address);
                phone = findViewById(R.id.tel_no);
                email = findViewById(R.id.email);
                city = findViewById(R.id.city);
                description = findViewById(R.id.description);
                profileImg = findViewById(R.id.seller_image1);

                //set text in edit text view with the seller data in database
                userName.setText(seller.getUser_name());
                shopName.setText(seller.getShop_name());
                address.setText(seller.getAddress());
                phone.setText(seller.getTel_no());
                email.setText(seller.getEmail());
                city.setText(seller.getCity());
                description.setText(seller.getDesc());

                //put the data into another empty variable(outside of this function) to pass to another activity
                seller_pass.setUser_name(seller.getUser_name());
                seller_pass.setShop_name(seller.getShop_name());
                seller_pass.setAddress(seller.getAddress());
                seller_pass.setTel_no(seller.getTel_no());
                seller_pass.setEmail(seller.getEmail());
                seller_pass.setCity(seller.getCity());
                seller_pass.setDesc(seller.getDesc());
                //Toast.makeText(context,"Success", Toast.LENGTH_LONG ).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Toast.makeText(context,"Failed to load data", Toast.LENGTH_LONG ).show();
                finish();
            }
        });

        //pass the seller_pass object along with its data to another activity
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(seller_profile.this, edit_profile_seller.class);
                intent.putExtra("seller_data", seller_pass);
                startActivity(intent);
            }
        });

        ItemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(seller_profile.this,item.class);
                startActivity(intent);
            }
        });
    }



}

