package com.example.jompasarseller;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FirestoreAdapter extends FirestoreRecyclerAdapter<order_model, FirestoreAdapter.orderViewHolder> {

    FirebaseStorage fStore;
    StorageReference storeRef;
    OnListItemClick onListItemClick;
    Context context;

    public FirestoreAdapter(Context c, @NonNull FirestoreRecyclerOptions<order_model> options, OnListItemClick onListItemClick) {
        super(options);
        this.onListItemClick = onListItemClick;
        this.context = c;
    }



    @NonNull
    @Override
    public orderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_item,parent,false);
        return new orderViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull orderViewHolder holder, int position, @NonNull order_model model) {
        //Firebase Variable
        String order_id = getSnapshots().getSnapshot(position).getId();

        Timestamp ts = model.getOrder_time();
        Date dt  = ts.toDate();

        //Display the Data
        holder.order_list_buyer_id.setText(model.getBuyer_id());
        holder.order_list_remark.setText(model.getRemark());
        holder.order_list_order_time.setText(String.valueOf(dt));
        holder.order_list_is_delivered.setText(String.valueOf(model.getIs_delivery()));
        holder.order_list_status.setText(String.valueOf(model.getStatus()));

    }

    public class orderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView  order_list_buyer_id;
        private TextView  order_list_remark;
        private TextView  order_list_order_time;
        private TextView  order_list_is_delivered;
        private TextView  order_list_status;


        public orderViewHolder(@NonNull View itemView) {
            super(itemView);

            //XML hook to Java Code;
            order_list_buyer_id = itemView.findViewById(R.id.order_list_buyer_id_display);
            order_list_remark = itemView.findViewById(R.id.order_list_remark_display);
            order_list_order_time = itemView.findViewById(R.id.order_list_time_display);
            order_list_is_delivered = itemView.findViewById(R.id.order_list_deliver_display);
            order_list_status = itemView.findViewById(R.id.order_list_status_display);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //This will pass the data to MainActivity;
            //onListItemClick.onItemClick(getItem(getAdapterPosition()),getAdapterPosition(), getSnapshots());
              onListItemClick.onItemClick(getSnapshots().getSnapshot(getAdapterPosition()).getId());
        }
    }

    //Interface used for the MainActivity
    public interface  OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String orderID);
    }
}

