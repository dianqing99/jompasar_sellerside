package com.example.jompasarseller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class edit_profile_seller extends AppCompatActivity {

    //Firebase variable
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth;
    FirebaseUser fUser;
    FirebaseStorage storage;
    StorageReference storeRef;

    //XML Bind Variable
    private TextView userName;
    private TextView shopName;
    private TextView address;
    private TextView phone;
    private TextView city;
    private TextView description;

    ImageView sellerImage;

    private ImageView profileImg;
    private Button btnupdate;
    private Button uploadProfilePicBtn;
    private String sellerID;
    private String NewUserName, NewShopName, NewAddress, NewPhone, NewCity, NewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_seller);

        //retrieve the object data pass from previous activity
        Intent i = getIntent();
        seller_model seller = (seller_model)i.getSerializableExtra("seller_data");

        //Instantiate Firebase Variable
        firebaseFirestore = FirebaseFirestore.getInstance();
        fAuth =FirebaseAuth.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();

        //allocate variable to respective view
        btnupdate = findViewById(R.id.seller_update);
        uploadProfilePicBtn =findViewById(R.id.upload_picture_btn);
        profileImg = findViewById(R.id.seller_image);
        userName = findViewById(R.id.NewUserName);
        shopName = findViewById(R.id.NewShopName);
        address = findViewById(R.id.NewAddress);
        phone = findViewById(R.id.NewPhoneNo);
        city = findViewById(R.id.NewCIty);
        description = findViewById(R.id.NewDesc);

        //set the seller data into the edit text view
        userName.setText(seller.getUser_name());
        shopName.setText(seller.getShop_name());
        address.setText(seller.getAddress());
        phone.setText(seller.getTel_no());
        city.setText(seller.getCity());
        description.setText(seller.getDesc());

        //Code for Displaying Image
        StorageReference profileImgRef = storeRef.child("/seller/" + fAuth.getCurrentUser().getUid()
                + "/seller_img.jpg");
        profileImgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(profileImg);
            }
        });


        uploadProfilePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open the Gallery
                //This will return the URL of the Image that the User choose
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(openGalleryIntent,1000);
            }
        });


        //update button click
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //locate specific seller file
                sellerID = fAuth.getCurrentUser().getUid().toString();
                DocumentReference seller_update_ref = firebaseFirestore.collection("sellers").document(sellerID);

                //assign the seller data value to the string variable
                NewUserName = userName.getText().toString().trim();
                NewShopName = shopName.getText().toString().trim();
                NewAddress = address.getText().toString().trim();
                NewPhone = phone.getText().toString().trim();
                NewCity = city.getText().toString().trim();
                NewDescription = description.getText().toString().trim();

                seller_update_ref.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.contains("desc") == false) {
                            //add field "desc" then add in the data
                            Map<String,Object> sellerProfile = new HashMap<>();
                            sellerProfile.put("desc",NewDescription);

                            //update data in database
                            seller_update_ref.update("user_name", NewUserName);
                            seller_update_ref.update("shop_name", NewShopName);
                            seller_update_ref.update("address", NewAddress);
                            seller_update_ref.update("tel_no", NewPhone);
                            seller_update_ref.update("city", NewCity);
                            //seller_update_ref.update("desc", NewDescription);
                        }
                        else{
                            //update data in database
                            seller_update_ref.update("user_name", NewUserName);
                            seller_update_ref.update("shop_name", NewShopName);
                            seller_update_ref.update("address", NewAddress);
                            seller_update_ref.update("tel_no", NewPhone);
                            seller_update_ref.update("city", NewCity);
                            seller_update_ref.update("desc", NewDescription);
                        }

                    }
                });

                //update data in database
                seller_update_ref.update("user_name", NewUserName);
                seller_update_ref.update("shop_name", NewShopName);
                seller_update_ref.update("address", NewAddress);
                seller_update_ref.update("tel_no", NewPhone);
                seller_update_ref.update("city", NewCity);
                seller_update_ref.update("desc", NewDescription);

                //pass back to seller profile activity

                Intent intent = new Intent(edit_profile_seller.this, seller_profile.class);
                //finish();
                startActivity(intent);
            }
        });

    }



    //Additional Functions
    //Let seller Select Picture by Opening the Gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1000){
            if(resultCode == Activity.RESULT_OK){
                Uri imageURI = data.getData();
                //Testing code to see if ImageView is working
                //profileImage.setImageURI(imageURI);
                uploadImageToFirebase(imageURI);
            }
        }
    }

    //This function will upload the Images to Firebase
    private void uploadImageToFirebase(Uri imageURI) {

        StorageReference fileRef = storeRef.child("/seller/" + fAuth.getCurrentUser().getUid() + "/seller_img.jpg");

        //Remember URI is references to the Image on the Device
        fileRef.putFile(imageURI).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(edit_profile_seller.this,"Profile Image Uploaded!", Toast.LENGTH_SHORT).show();

                fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.get().load(uri).into(profileImg);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(edit_profile_seller.this,"Unable to Retrieve Profile Image from Database!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(edit_profile_seller.this,"Fail to Upload Profile Image!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
