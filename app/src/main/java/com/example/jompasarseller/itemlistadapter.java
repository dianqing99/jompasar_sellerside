package com.example.jompasarseller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class itemlistadapter extends RecyclerView.Adapter<itemlistadapter.MyViewHolder> {

    private OnListItemClick listener;
    ArrayList<sellermenu> menu;
    Context context;

    public itemlistadapter(Context c, ArrayList<sellermenu> sm, OnListItemClick onListItemClick){
        context = c;
        this.listener = onListItemClick;
        menu = sm;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(menu.get(position).getName());
        holder.description.setText(menu.get(position).getDescription());
        holder.price.setText("From " + menu.get(position).getPrice().toString());

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name, description, price;
        ImageView editButton, deleteButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nametext);
            description = itemView.findViewById(R.id.descriptiontext);
            price = itemView.findViewById(R.id.pricetext);

            editButton = itemView.findViewById(R.id.editicon);
            deleteButton = itemView.findViewById(R.id.deleteicon);

            editButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);
//            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String buttonType = "";
            switch (v.getId()) {
                case R.id.editicon:
                    buttonType = "edit";
                    break;
                case R.id.deleteicon:
                    buttonType = "delete";
                    break;
                default:
                    buttonType = "failed";
                    break;
            }

            String s = name.getText().toString();
            listener.onItemClick(buttonType,s);
        }
    }

    public interface OnListItemClick {
        void onItemClick(String buttonType, String Name);
    }

}
