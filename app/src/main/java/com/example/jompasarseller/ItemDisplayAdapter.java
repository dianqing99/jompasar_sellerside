package com.example.jompasarseller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;

public class ItemDisplayAdapter extends RecyclerView.Adapter<ItemDisplayAdapter.MyViewHolder> {

    ArrayList<order_menu_model> menu;
    Context context;

    public ItemDisplayAdapter(Context c, ArrayList<order_menu_model> itemdetail){
        context = c;
        menu = itemdetail;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_display,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(menu.get(position).getName());
        holder.id.setText(menu.get(position).getId());
        holder.quantity.setText(menu.get(position).getQuantity().toString());
        holder.unitPrice.setText("RM " + menu.get(position).getUnit_price().toString());

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name, quantity, unitPrice, id;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            id = itemView.findViewById(R.id.id);
            quantity = itemView.findViewById(R.id.quantity);
            unitPrice = itemView.findViewById(R.id.unitPrice);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //listener.onClick(view,getAdapterPosition());

        }
    }

    /*public interface RecyclerViewClickListener
    {
        void onClick(View v,int position);
    }*/

}
