package com.example.jompasarseller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;

public class item extends AppCompatActivity implements itemlistadapter.OnListItemClick{

    private FirebaseFirestore firebaseFirestore;
    private RecyclerView ItemList;
    private Context context = this;

    StorageReference storeRef;
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    FloatingActionButton fb;
    ImageView image;

    private ArrayList<sellermenu> menuList;
    private seller_model seller;
    private itemlistadapter adapter;
    private String SellerId;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            menuList = (ArrayList<sellermenu>) data.getSerializableExtra("menuList");
            ItemList.setAdapter(new itemlistadapter(context, menuList, (itemlistadapter.OnListItemClick) context));
            ItemList.invalidate();
//            adapter.notifyItemInserted(menuList.size()-1);
            Toast.makeText(context, "Successfully added Menu item", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setAdapter();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        firebaseFirestore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();

        ItemList = findViewById(R.id.itemlist);

        fAuth = FirebaseAuth.getInstance();
        SellerId = fAuth.getCurrentUser().getUid().toString();

        fb = (FloatingActionButton) findViewById(R.id.fadd);
        fb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, additemmodule.class);
                intent.putExtra("menuList", (Serializable) menuList);
                startActivityForResult(intent, 1);
//                startActivity(new Intent(getApplicationContext(), additemmodule.class));
            }
        });

        // Query
        DocumentReference docRef = firebaseFirestore.collection("sellers").document(SellerId);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                seller = documentSnapshot.toObject(seller_model.class);
                menuList = seller.getMenu();
                // Set adapter
                adapter = new itemlistadapter(context, menuList, (itemlistadapter.OnListItemClick) context);
                ItemList.setAdapter(adapter);
                ItemList.setLayoutManager(new LinearLayoutManager(context));
                ItemList.setHasFixedSize(false);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Failed to load data", Toast.LENGTH_SHORT).show();
                finish();
            }
        });



    }


    @Override
    public void onItemClick(String buttonType, String Name) {
        Integer position = checkOrderExist(Name);

        if(buttonType == "edit"){
            Toast.makeText(context, "Edit", Toast.LENGTH_SHORT).show();
        }else if(buttonType == "delete"){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            sellermenu tempMenu = menuList.get(position);
                            menuList.remove(tempMenu);

                            seller.setMenu(menuList);
                            fStore = FirebaseFirestore.getInstance();
                            fStore.collection("sellers").document(SellerId).set(seller);

                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(position, menuList.size());
                            adapter.notifyDataSetChanged();

                            Toast.makeText(context, "Successfully Deleted the Item", Toast.LENGTH_SHORT).show();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you sure you want to delete this item?")
                    .setNegativeButton("No", dialogClickListener)
                    .setPositiveButton("Yes", dialogClickListener)
                    .show();
        }
    }

    public int checkOrderExist(String itemName) {
        if( !this.menuList.isEmpty() ){
            for (int i = 0; i < this.menuList.size(); i++) {
                if (this.menuList.get(i).getName().equals(itemName)) {
                    return i;
                }
            }
        }
        return 999999999;
    }
}

