package com.example.jompasarseller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class splash_screen extends AppCompatActivity {

    private static int SPLASH_SCREEN = 3000;

    //Variable
    Animation topAnim, bottomAnim;
    ImageView logo;
    TextView slogan,message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        //Animations
        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_anim);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_anim);

        //XML Hooks
        logo = findViewById(R.id.splash_logo);
        message = findViewById(R.id.spalsh_message);

        //Assign Animation to element
        logo.setAnimation(topAnim);
        message.setAnimation(bottomAnim);

         new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(splash_screen.this, login_seller.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_SCREEN);


    }
}