package com.example.jompasarseller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FirestoreAdapter.OnListItemClick {
    //Global Variable
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navView;
    String seller_id;

    order_model order = new order_model();

    //Firebase Variable
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    StorageReference storeRef;

    //Firestore UI Variable
    private RecyclerView fireStoreOrderList;
    private FirestoreAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Firebase Variable Initialization
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        seller_id = fAuth.getCurrentUser().getUid();

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        navView=findViewById(R.id.nav_view);
        navView.bringToFront();
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        navView.setNavigationItemSelectedListener(this);

        //When User is Logged in, Then only show the profile and Logout, and Hide Login.
        Menu menu = navView.getMenu();
        //menu.findItem(R.id.nav_login).setVisible(false);
        menu.findItem(R.id.nav_logout).setVisible(true);
        menu.findItem(R.id.nav_profile).setVisible(true);

        //Display the Order Details Here
        //Use Recycle View here.


        //Firestore UI RecycleView Logic
        //Query and Variable Initialization
        fireStoreOrderList = findViewById(R.id.firestore_order_list);

        Query query = fStore.collection("orders").whereEqualTo("seller_id",seller_id);

        //Recycler Options
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<order_model>()
                .setQuery(query,order_model.class)
                .build();

        adapter = new FirestoreAdapter(this,options, (FirestoreAdapter.OnListItemClick) this);

        //View Holder Class
        fireStoreOrderList.setHasFixedSize(true);
        fireStoreOrderList.setLayoutManager(new LinearLayoutManager(this));
        fireStoreOrderList.setAdapter(adapter);



    }

    //----------------------------------------------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()){
            case R.id.nav_profile:
                Intent profile_intent = new Intent(MainActivity.this,seller_profile.class);
                startActivity(profile_intent);
                break;
            case R.id.nav_logout:
                fAuth.signOut();
                Toast.makeText(this, "Loging off Users", Toast.LENGTH_SHORT).show();
                Log.d("Logout_Success","Loging off Users");
                Intent sign_out = new Intent(MainActivity.this, login_seller.class);
                startActivity(sign_out);
                break;
            case R.id.nav_item:
                //Create Intent to pass to Item
                Intent intent=new Intent(MainActivity.this,item.class);
                startActivity(intent);
                break;
            case R.id.nav_review:
                //Create Intent for Review Page
                Intent review_intent = new Intent(MainActivity.this, review_list.class);
                startActivity(review_intent);
                break;

        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onItemClick(String orderID) {
        Toast.makeText(this, orderID, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(MainActivity.this,order_item_detail.class);
        i.putExtra("orderid", orderID);
        startActivity(i);
    }





}