package com.example.jompasarseller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class signup_seller extends AppCompatActivity {

    EditText seller_signUpUsername, seller_signupPassword, seller_signupEmail, seller_signupTelno, seller_signupShopname, seller_signupAddr, seller_signupCity;
    Button seller_uploadProfilePicBtn, seller_submitBtn, seller_loginBtn;
    String sellerUserID;


    //Firebase Variable
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    StorageReference storeRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_seller);

        //Bind Java Code with XML Layout
        seller_signUpUsername = findViewById(R.id.seller_signup_username);
        seller_signupPassword = findViewById(R.id.seller_signup_password);
        seller_signupEmail = findViewById(R.id.seller_signup_email);
        seller_signupTelno = findViewById(R.id.seller_signup_phoneNo);
        seller_signupShopname = findViewById(R.id.seller_signup_shopName);
        seller_signupAddr = findViewById(R.id.seller_signup_shopName);
        seller_submitBtn = findViewById(R.id.seller_signup_submit);
        seller_loginBtn = findViewById(R.id.seller_Login);
        seller_signupCity = findViewById(R.id.seller_signup_city);

        //Instantiate Firebase Obj
        fAuth =FirebaseAuth.getInstance();
        fStore =FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();



        seller_submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get Details
                String sellerUsername = seller_signUpUsername.getText().toString();
                String sellerPassword = seller_signupPassword.getText().toString();
                String sellerEmail = seller_signupEmail.getText().toString();
                String sellerTelno = seller_signupTelno.getText().toString();
                String sellerShopname = seller_signupShopname.getText().toString();
                String sellerAddr = seller_signupAddr.getText().toString();
                String sellerCity = seller_signupCity.getText().toString();


                fAuth.createUserWithEmailAndPassword(sellerEmail,sellerPassword).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Toast.makeText(signup_seller.this, "Account Register Succesfully!", Toast.LENGTH_SHORT).show();

                        //Firestore Code
                        //Getting the User ID and Creating a Document at buyers collection on Firebase
                        sellerUserID = fAuth.getCurrentUser().getUid();
                        DocumentReference docRef = fStore.collection("sellers").document(sellerUserID);

                        //Initialize Some Variable so it doesn't cause crash later

                        ArrayList<Map> menu = new ArrayList<>();
                        ArrayList<Map> ratedbuyer = new ArrayList<>();

                        //Put Buyers Details to HashMap
                        Map<String,Object> sellerProfile = new HashMap<>();
                        sellerProfile.put("user_name",sellerUsername);
                        sellerProfile.put("email",sellerEmail);
                        sellerProfile.put("tel_no",sellerTelno);
                        sellerProfile.put("shop_name",sellerShopname);
                        sellerProfile.put("address",sellerAddr);
                        sellerProfile.put("city", sellerCity);
                        sellerProfile.put("avg_rating", 0);
                        sellerProfile.put("rating_count", 0);
                        sellerProfile.put("img_url", "seller_img");
                        sellerProfile.put("menu",menu);
                        sellerProfile.put("rated_buyer",ratedbuyer);


                        //Setting New Buyer Profile on Firestore Document
                        docRef.set(sellerProfile).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(signup_seller.this, "User Profile is Created!", Toast.LENGTH_SHORT).show();
                                Log.d("ProfileMsg_Success","User Profile is Created!");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(signup_seller.this, "Failure on Creating User Profile!", Toast.LENGTH_SHORT).show();
                                Log.d("ProfileMsg_Failure","Failure on Creating User Profile");
                            }
                        });

                        //Send User to Login Page
                        startActivity(new Intent(getApplicationContext(),login_seller.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(signup_seller.this, "Error on Registering New Account!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        seller_loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),login_seller.class));
            }
        });

    }
}