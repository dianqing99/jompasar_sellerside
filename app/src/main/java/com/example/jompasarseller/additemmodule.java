package com.example.jompasarseller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class additemmodule extends AppCompatActivity {
    //Global UI Variable
    EditText name, description,price;
    Button submit, back;

    //Global Firebase Variable
    private FirebaseFirestore fStore;
    private Context context = this;
    private ArrayList<sellermenu> menuList;

    DocumentReference sellerRef;
    FirebaseAuth fAuth;
    String seller_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additemmodule);

        //Firebase Intilization;
        fAuth = FirebaseAuth.getInstance();
        seller_id = fAuth.getCurrentUser().getUid();
        fStore= FirebaseFirestore.getInstance();
        sellerRef = fStore.document("/sellers/" + seller_id);

        Intent intent = getIntent();
        menuList = (ArrayList<sellermenu>) intent.getSerializableExtra("menuList");

        //Back Button Logic
        back=(Button)findViewById(R.id.add_back);
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), item.class));

            }
        });

        //Submit Button Logic
        submit=(Button)findViewById(R.id.add_submit);
        submit.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void  onClick(View view)
            {
                processinsert();
            }
        });

    }

    private void processinsert()
    {
        //XML Hooks
        name=(EditText)findViewById(R.id.add_name);
        description=(EditText)findViewById(R.id.add_description);
        price=(EditText)findViewById(R.id.add_price);

        sellermenu menu = new sellermenu();
        menu.setName(name.getText().toString());
        menu.setDescription(description.getText().toString());
        menu.setPrice(Double.parseDouble(price.getText().toString()));
        menuList.add(menu);

        Map<String,Object> map=new HashMap<>();
        map.put("name",name.getText().toString());
        map.put("description",description.getText().toString());
        map.put("price",Integer.parseInt(price.getText().toString()));

        //Use this to update the array in menu
        sellerRef.update("menu", FieldValue.arrayUnion(map)).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Intent output = new Intent();
                output.putExtra("menuList",(Serializable) menuList);
                setResult(RESULT_OK, output);
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Fail to add Menu item", Toast.LENGTH_SHORT).show();
            }
        });
    }
}


