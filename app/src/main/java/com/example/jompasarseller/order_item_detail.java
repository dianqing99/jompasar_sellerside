package com.example.jompasarseller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Date;

public class order_item_detail extends AppCompatActivity {

    FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth;

    TextView order_id, buyer_id, remark_textview, order_time_textview, is_delivery_textview, is_review_textview, status_textview;
    TextView item_id, item_name, item_quantity, item_unit_price;
    TextView total_price;
    TextView Status;

    Button status, deliver;

    private RecyclerView item_detail_view;

    private Context context = this;

    String orderID, NewStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item_detail);

        Intent i = getIntent();
        orderID = i.getStringExtra("orderid");

        firebaseFirestore = FirebaseFirestore.getInstance();
        fAuth =FirebaseAuth.getInstance();

        item_detail_view = findViewById(R.id.item_recycler_list);
        status = findViewById(R.id.status_update);
        deliver = findViewById(R.id.delivery_status_update);

        DocumentReference docRef = firebaseFirestore.collection("orders").document(orderID);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                order_model order = documentSnapshot.toObject(order_model.class);
                ArrayList<order_menu_model> item = order.getMenu_items();

                order_id = findViewById(R.id.orderID);
                buyer_id = findViewById(R.id.buyerID);
                remark_textview = findViewById(R.id.remark);
                order_time_textview = findViewById(R.id.orderTime);
                is_delivery_textview = findViewById(R.id.isDelivery);
                is_review_textview = findViewById(R.id.isReviewed);
                status_textview = findViewById(R.id.status);

                item_id = findViewById(R.id.id);
                item_name = findViewById(R.id.name);
                item_quantity = findViewById(R.id.quantity);
                item_unit_price = findViewById(R.id.unitPrice);

                total_price = findViewById(R.id.totalPrice);

                Timestamp ts = order.getOrder_time();
                Date dt  = ts.toDate();

                order_id.setText(orderID);
                buyer_id.setText(order.getBuyer_id());
                remark_textview.setText(order.getRemark());
                order_time_textview.setText(String.valueOf(dt));
                is_delivery_textview.setText(String.valueOf(order.getIs_delivery()));
                is_review_textview.setText(String.valueOf(order.getIs_reviewed()));
                status_textview.setText(order.getStatus());


                ItemDisplayAdapter itemadapter = new ItemDisplayAdapter(context, order.getMenu_items());
                item_detail_view.setAdapter(itemadapter);
                item_detail_view.setLayoutManager(new LinearLayoutManager(context));
                item_detail_view.setHasFixedSize(true);

                total_price.setText("RM " + String.valueOf(order.getTotal_price()));
            }
        });

        Status = findViewById(R.id.editStatus);

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocumentReference docRef = firebaseFirestore.collection("orders").document(orderID);

                NewStatus = Status.getText().toString().trim();

                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        order_model order = documentSnapshot.toObject(order_model.class);

                        docRef.update("status", NewStatus);

                    }
                });
                finish();
            }

        });

        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocumentReference docRef = firebaseFirestore.collection("orders").document(orderID);

                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        order_model order = documentSnapshot.toObject(order_model.class);

                        if(order.getIs_delivery() == true){
                            docRef.update("is_delivery", false);
                        }else{
                            docRef.update("is_delivery", true);
                        }
                    }
                });
                finish();
            }

        });


    }
}