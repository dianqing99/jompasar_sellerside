package com.example.jompasarseller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login_seller extends AppCompatActivity {

    //Global Variable
    EditText seller_loginEmail, seller_loginPassword;
    Button seller_loginBtn, seller_loginResetPass, seller_signUpBtn;
    FirebaseAuth login_fAuth;
    AlertDialog.Builder ForgetPass_Alert;
    LayoutInflater inflater;
    FirebaseUser user;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Toast.makeText(this,"HOHO",Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_seller);

        seller_loginEmail = findViewById(R.id.seller_loginEmail);
        seller_loginPassword = findViewById(R.id.seller_loginPass);
        seller_loginBtn = findViewById(R.id.seller_loginBtn);
        seller_signUpBtn = findViewById(R.id.seller_login_signupBtn);
        seller_loginResetPass = findViewById(R.id.seller_loginResetPass);

        login_fAuth=FirebaseAuth.getInstance();
        user = login_fAuth.getCurrentUser();
        if(user!=null){
            Toast.makeText(this,"Login Successfully",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
        ForgetPass_Alert = new AlertDialog.Builder(this);
        inflater = this.getLayoutInflater();


        //Button Logic on Buyer Login Activity
        seller_loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Validate Details
                if(seller_loginEmail.getText().toString().isEmpty()){
                    seller_loginEmail.setError("Email is Missing");
                    return;
                }

                if(seller_loginPassword.getText().toString().isEmpty()){
                    seller_loginPassword.setError("Password is Missing");
                    return;
                }

                //Extract Details
                String sellerEmail = seller_loginEmail.getText().toString();
                String sellerUserPass = seller_loginPassword.getText().toString();

                //Login in Logic
                login_fAuth.signInWithEmailAndPassword(sellerEmail,sellerUserPass).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Toast.makeText(login_seller.this,"Login Successfully",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(login_seller.this,"Login Failure",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        seller_signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Sent it to Sign Up Activity
                startActivity(new Intent(getApplicationContext(),signup_seller.class));
            }
        });

        seller_loginResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start Alert Dialog
                View view =inflater.inflate(R.layout.reset_popup,null);
                ForgetPass_Alert.setTitle("Reset Password?")
                        .setMessage("Enter your Registered Email to Get the Reset Link")
                        .setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                //Extract and Validate
                                EditText email = view.findViewById(R.id.reset_email_popup);
                                if(email.getText().toString().isEmpty()){
                                    //This will cause error, Check
                                    email.setError("This Field is Required");
                                    return;
                                }

                                //Send the Reset Link
                                login_fAuth.sendPasswordResetEmail(email.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(login_seller.this,"Email Sent!",Toast.LENGTH_LONG).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(login_seller.this,e.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("Cancel",null).setView(view).create().show();

            }
        });
    }

}

